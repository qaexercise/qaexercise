/**
 * This package is containing the test cases for Registration feature
 * @author Luca Dragos
 */
package com.gamesys.qaexercise.registration.tests;