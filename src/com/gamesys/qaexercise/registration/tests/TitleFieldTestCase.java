package com.gamesys.qaexercise.registration.tests;

import static org.junit.Assert.*;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.gamesys.qaexercise.utilities.QAExerciseUtilities;
import com.gamesys.qaexercise.webpages.RegistrationPage;

/***
 * Test cases for field Title from Registration page
 * @author Luca Dragos
 */
public class TitleFieldTestCase 
{
	/***
	 * Warming up the web driver with the web page under test 
	 * @throws ConfigurationException
	 */
	@Before
	public void init() throws ConfigurationException
	{
		m_webDriver = QAExerciseUtilities.getDefaultWebDriver();
		m_webDriver.get("https://www.jackpotjoy.com/register");		
	}

	/***
	 * Testing for the Title field to be present in the page and to have default value Select Title
	 */
	@Test
	public void titleFieldPresence_test() 
	{
		try
		{
			final RegistrationPage page = PageFactory.initElements(m_webDriver, RegistrationPage.class);
			final Select titleSelect = new Select(page.getTitle());
			final String titleTextValue = titleSelect.getFirstSelectedOption().getText();
			
			assertTrue("Title field was not found on the page.",titleTextValue.equals("Select Title"));
		}
		catch(org.openqa.selenium.NoSuchElementException e)
		{
			fail("Title field not found.");
		}						 
	}
	
	/***
	 * Testing for Title field to be able to select value Miss
	 */
	@Test
	public void titleField_01_test() 
	{
		try
		{
			final String MISS="Miss";								
		
			final String titleTextValue = setAndGetTitleValue(MISS);									
			assertTrue("Unexpected value for field Title.",titleTextValue.equals(MISS));
		}
		catch(org.openqa.selenium.NoSuchElementException e)
		{
			fail("Expected value not found in Title field options.");
		}	
	}
	
	/***
	 * Testing for Title field to be able to select value Mrs
	 */
	@Test
	public void titleField_02_test() 
	{
		try
		{
			final String MRS="Mrs";						
			
			final String titleTextValue = setAndGetTitleValue(MRS);									
			assertTrue("Unexpected value for field Title.",titleTextValue.equals(MRS));
		}
		catch(org.openqa.selenium.NoSuchElementException e)
		{
			fail("Expected value not found in Title field options.");
		}
	}
	
	/***
	 * Testing for Title field to be able to select value Ms
	 */
	@Test
	public void titleField_03_test() 
	{
		try
		{
			final String MS="Ms";						
			
			final String titleTextValue = setAndGetTitleValue(MS);										
			assertTrue("Unexpected value for field Title.",titleTextValue.equals(MS));
		}
		catch(org.openqa.selenium.NoSuchElementException e)
		{
			fail("Expected value not found in Title field options.");
		}
	}
	
	/***
	 * Testing for Title field to be able to select value Mr
	 */
	@Test
	public void titleField_04_test() 
	{						
		try
		{
			final String MR="Mr";
			
			final String titleTextValue = setAndGetTitleValue(MR);
			assertTrue("Unexpected value for field Title.",titleTextValue.equals(MR));
		}
		catch(org.openqa.selenium.NoSuchElementException e)
		{
			fail("Expected value not found in Title field options.");
		}
	}
	
	/***
	 * Testing for Title field to be able to select value Dr
	 */
	@Test
	public void titleField_05_test() 
	{							
		try
		{
			final String DR="Dr";
			
			final String titleTextValue = setAndGetTitleValue(DR);
			assertTrue("Unexpected value for field Title.",titleTextValue.equals(DR));
		}
		catch(org.openqa.selenium.NoSuchElementException e)
		{
			fail("Expected value not found in Title field options.");
		}
	}
	
	/***
	 * Testing for Title field to be able to select value Prof
	 */
	@Test
	public void titleField_06_test() 
	{							
		try
		{
			final String PROF="Prof";
			
			final String titleTextValue = setAndGetTitleValue(PROF);
			assertTrue("Unexpected value for field Title.",titleTextValue.equals(PROF));		
		}
		catch(org.openqa.selenium.NoSuchElementException e)
		{
			fail("Expected value not found in Title field options.");
		}
	}
	
	/***
	 * Testing for Title field for number of available the options
	 */
	@Test
	public void titleField_07_test() 
	{			
		final RegistrationPage page = PageFactory.initElements(m_webDriver, RegistrationPage.class);				
		final Select titleSelect = new Select(page.getTitle());
		
		final int optionCount = titleSelect.getOptions().size();
		
		assertTrue("Unexpected option count.",optionCount == 6);
	}
	
	/***
	 * Testing for Title field with a random set of steps
	 */
	@Test
	public void titleField_08_test() 
	{			
		try
		{					
			setAndGetTitleValue("Miss");
			setAndGetTitleValue("Mr");
			//keeping last value for evaluation
			final String titleTextValue = setAndGetTitleValue("Miss");
			
			assertTrue("Unexpected value for field Title",titleTextValue.equals("Miss"));		
		}
		catch(org.openqa.selenium.NoSuchElementException e)
		{
			fail("Expected value not found in Title field options.");
		}
	}
	
	/***
	 * Test utility method to set a value to Title field and return the selected text
	 * @param value String
	 * @return String
	 */
	private String setAndGetTitleValue(final String value) throws org.openqa.selenium.NoSuchElementException
	{
		try
		{
			final RegistrationPage page = PageFactory.initElements(m_webDriver, RegistrationPage.class);				
			final Select titleSelect = new Select(page.getTitle());
			titleSelect.selectByVisibleText(value);				
			page.getTitle().click();
		
			final String titleTextValue = titleSelect.getFirstSelectedOption().getText();
				
			return titleTextValue;
		}
		catch(org.openqa.selenium.NoSuchElementException e)
		{
			throw e;
		}		
	}
	
	/***
	 * Cleaning up by shutting down the web driver
	 */
	@After
	public void tearDown()
	{
		m_webDriver.quit();
	}
	
	private WebDriver m_webDriver;
}
