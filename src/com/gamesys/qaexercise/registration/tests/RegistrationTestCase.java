package com.gamesys.qaexercise.registration.tests;

import static org.junit.Assert.*;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.gamesys.qaexercise.actors.UserActor;
import com.gamesys.qaexercise.actors.UserActorBuilder;
import com.gamesys.qaexercise.utilities.QAExerciseUtilities;
import com.gamesys.qaexercise.webpages.RegistrationPage;

/***
 * Test cases for full registration steps
 * @author Luca Dragos
 */
public class RegistrationTestCase 
{
	/***
	 * Warming up the web driver with the web page under test 
	 * @throws ConfigurationException
	 */
	@Before
	public void init() throws ConfigurationException
	{
		m_webDriver = QAExerciseUtilities.getDefaultWebDriver();
		m_webDriver.get("https://www.jackpotjoy.com/register");		
	}

	/***
	 * Test of the registration steps
	 */
	@Test
	public void full_registration_test() 
	{
		UserActor userActor = null;
		try 
		{
			userActor = UserActorBuilder.createNewUserActor();
		} 
		catch (ConfigurationException e) 
		{
			fail("Failed to read user seed information from configuration file.");
		}
		
		final RegistrationPage regPage = PageFactory.initElements(m_webDriver, RegistrationPage.class);
		//username
		regPage.getUserName().clear();
		regPage.getUserName().sendKeys(userActor.getUsername());
		
		//password
		regPage.getPassword().clear();
		regPage.getPassword().sendKeys(userActor.getPassword());
		regPage.getPasswordConfirmation().clear();
		regPage.getPasswordConfirmation().sendKeys(userActor.getPassword());
		
		//mail
		regPage.getEmail().clear();
		regPage.getEmail().sendKeys(userActor.getMail());
		
		if (regPage.getConfirmEmail().isDisplayed())
		{
			regPage.getConfirmEmail().clear();
			regPage.getConfirmEmail().sendKeys(userActor.getMail());
		}
		
		//user info		
		new Select(regPage.getTitle()).selectByVisibleText("Mrs");
		regPage.getTitle().click();
		
		regPage.getFirstName().clear();
		regPage.getFirstName().sendKeys(userActor.getGenericName());
		regPage.getSurname().clear();
		regPage.getSurname().sendKeys(userActor.getGenericName());
		
		//age
		new Select(regPage.getDayOfBirth()).selectByVisibleText("1");
		regPage.getDayOfBirth().click();
		
		new Select(regPage.getMonthOfBirth()).selectByVisibleText("Jan");
		regPage.getMonthOfBirth().click();
		
		new Select(regPage.getYearOfBirth()).selectByVisibleText("1980");
		regPage.getYearOfBirth().click();
		
		//postal code
		regPage.getPostalCode().click();
		regPage.getPostalCode().sendKeys(userActor.getLondonPostalCode());
		
		regPage.getFindAddressButton().click();
		
		//phone
		regPage.getPhoneNumber().clear();
		regPage.getPhoneNumber().sendKeys(userActor.getLondonPhoneNumber());
		
		//security Answer
		regPage.getSecurityAnswer();
		regPage.getSecurityAnswer().sendKeys(userActor.getMaidenName());
		
		//term and conditions
		regPage.getTermsAndConditionsCheck().click();
		
		//submiting registration
		regPage.getSubmitButton().click();
		
		try
		{
			assertTrue("Unexpexted behavior, registration failed.",m_webDriver.findElement(By.id("welcomeText")).getText().equals("Welcome,"));
		}
		catch(org.openqa.selenium.NoSuchElementException e)
		{
			fail("Expected element not found on page. Registration failed");
		}
	}
	/***
	 * Cleaning up by shutting down the web driver
	 */
	@After
	public void tearDown()
	{
		m_webDriver.quit();
	}

	private WebDriver m_webDriver;
}
