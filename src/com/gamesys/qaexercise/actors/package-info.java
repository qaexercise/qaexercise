/**
 * This package is containing the object actors used in the tests from QA Exercise
 * @author Luca Dragos
 */
package com.gamesys.qaexercise.actors;