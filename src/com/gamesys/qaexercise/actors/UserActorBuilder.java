package com.gamesys.qaexercise.actors;

import java.util.Random;

import org.apache.commons.configuration.ConfigurationException;

import com.gamesys.qaexercise.utilities.ConfigurationReader;
import com.gamesys.qaexercise.utilities.QAExerciseConstans;

/***
 * This class implements the builder pattern. It creates and fills UserActors objects.
 * @author Luca Dragos
 */
public class UserActorBuilder 
{
	public static UserActor createNewUserActor() throws ConfigurationException
	{
		final UserActor userActor = new UserActor();
		final String userSeedId=userSeedId();
		
		//creating an unique username
		final String username = ConfigurationReader.getInstance().getString(QAExerciseConstans.SEED_USERNAME_KEY,"");
		userActor.setUsername(username+userSeedId);
		
		userActor.setGenericName(username);
		
		//set the password
		userActor.setPassword(ConfigurationReader.getInstance().getString(QAExerciseConstans.SEED_PASSWORD_KEY,""));
		
		//setting the mail
		final String mail = ConfigurationReader.getInstance().getString(QAExerciseConstans.SEED_MAIL_KEY,"");
		userActor.setMail(username+userSeedId+mail);
		
		//set the phone
		userActor.setLondonPhoneNumber(ConfigurationReader.getInstance().getString(QAExerciseConstans.LONDON_PHONE_NUMBER_KEY,""));
		//set the postal code
		userActor.setLondonPostalCode(ConfigurationReader.getInstance().getString(QAExerciseConstans.LONDON_POSTAL_CODE_KEY,""));
		//set mainden name
		userActor.setMaidenName(ConfigurationReader.getInstance().getString(QAExerciseConstans.MAIDEN_NAME_KEY,""));
				
		return userActor;
	}

	/***
	 * Generates an user id based on the configuration seed interval
	 * @return String
	 * @throws ConfigurationException
	 */
	static public String userSeedId() throws ConfigurationException
	{			
		final int lowerLimit = ConfigurationReader.getInstance().getInt(QAExerciseConstans.SEED_LOWER_LIMIT_KEY,QAExerciseConstans.DEFAULT_LOWER_LIMIT);
		final int upperLimit = ConfigurationReader.getInstance().getInt(QAExerciseConstans.SEED_UPPER_LIMIT_KEY,QAExerciseConstans.DEFAULT_UPPER_LIMIT);		
		
		return Integer.toString(random.nextInt(upperLimit-lowerLimit) + lowerLimit);
	}
	
	private static Random random = new Random();
}
