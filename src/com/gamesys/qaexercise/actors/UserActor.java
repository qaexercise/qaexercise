package com.gamesys.qaexercise.actors;

/***
 * Abstractization of an user in form of an actor class
 * @author Luca Dragos
 */
public class UserActor 
{		
	public String getUsername() 
	{
		return m_username;
	}
	public void setUsername(String m_username) 
	{
		this.m_username = m_username;
	}
	
	public String getPassword() 
	{
		return m_password;
	}
	public void setPassword(String m_password) 
	{
		this.m_password = m_password;
	}
	
	public String getMail() 
	{
		return m_mail;
	}
	public void setMail(String m_mail) 
	{
		this.m_mail = m_mail;
	}
	
	public String getMaidenName() 
	{
		return m_maidenName;
	}
	public void setMaidenName(String m_maidenName) 
	{
		this.m_maidenName = m_maidenName;
	}
	
	public String getLondonPostalCode() 
	{
		return m_londonPostalCode;
	}
	public void setLondonPostalCode(String m_londonPostalCode) 
	{
		this.m_londonPostalCode = m_londonPostalCode;
	}
	
	public String getLondonPhoneNumber() 
	{
		return m_londonPhoneNumber;
	}
	public void setLondonPhoneNumber(String m_londonPhoneNumber) 
	{
		this.m_londonPhoneNumber = m_londonPhoneNumber;
	}
	
	public String getGenericName()
	{
		return this.m_genericName;
	}
	public void setGenericName(final String name)
	{
		this.m_genericName = name;
	}
	
	
	
	private String m_username;
	private String m_password;
	private String m_genericName;
	private String m_mail;
	private String m_maidenName;
	private String m_londonPostalCode;
	private String m_londonPhoneNumber;
}
