package com.gamesys.qaexercise.support.tests;

import static org.junit.Assert.*;

import java.util.Set;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.gamesys.qaexercise.utilities.QAExerciseUtilities;
import com.gamesys.qaexercise.webpages.SupportChatPage;
import com.gamesys.qaexercise.webpages.SupportPage;

/***
 * Test cases for Chat feature of support
 * @author Luca Dragos
 */
public class SuportChatTestCase 
{
	/***
	 * Warming up the web driver with the web page under test 
	 * @throws ConfigurationException
	 */
	@Before
	public void init() throws ConfigurationException
	{
		m_webDriver = QAExerciseUtilities.getDefaultWebDriver();
		m_webDriver.get("http://www.jackpotjoy.com/help/help");		
	}

	/***
	 * Testing for live chat window and fields: first name, last name, mail 
	 */
	@Test
	public void liveChat_test() 
	{	
		final String  mainWinhandle= m_webDriver.getWindowHandle();
		
		final SupportPage supportPage = PageFactory.initElements(m_webDriver, SupportPage.class);
		supportPage.getLiveChatButton().click();		
		
		final Set<String> handles = m_webDriver.getWindowHandles();
		String newWindowHandle = "";
		for(String hndl : handles)
		{
			if (!mainWinhandle.equals(hndl))
			{
				newWindowHandle = hndl;
			}
		}
		
		try
		{
			m_webDriver.switchTo().window(newWindowHandle);
		}
		catch(NoSuchWindowException e)
		{
			fail("Chat window not found");
		}
		
		try
		{
			final SupportChatPage chatPage = PageFactory.initElements(m_webDriver, SupportChatPage.class);
			chatPage.getFirstNameField().clear();
			chatPage.getFirstNameField().sendKeys("test");
			
			chatPage.getLastNameField().clear();
			chatPage.getLastNameField().sendKeys("test");
			
			chatPage.getEmailField().clear();
			chatPage.getEmailField().sendKeys("test");
		}
		catch(org.openqa.selenium.NoSuchElementException e)
		{
			fail("Chat window elements not found.");
		}
		
		//if we manage to get here the test is finished successfully
		assertTrue(true);
	}
	
	/***
	 * Cleaning up by shutting down the web driver
	 */
	@After
	public void tearDown()
	{
		m_webDriver.quit();
	}
	
	private WebDriver m_webDriver;

}
