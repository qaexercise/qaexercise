package com.gamesys.qaexercise.authentication.tests;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.gamesys.qaexercise.utilities.ConfigurationReader;
import com.gamesys.qaexercise.utilities.QAExerciseConstans;
import com.gamesys.qaexercise.utilities.QAExerciseUtilities;
import com.gamesys.qaexercise.webpages.AuthenticationPage;
import com.gamesys.qaexercise.webpages.LoginPage;
import com.gamesys.qaexercise.webpages.MainPage;

/***
 * Test cases for authentication feature
 * @author Luca Dragos
 */
public class AuthenticationTestCase 
{
	/***
	 * Warming up the web driver with the web page under test 
	 * @throws ConfigurationException
	 */
	@Before
	public void init() throws ConfigurationException
	{
		m_webDriver = QAExerciseUtilities.getDefaultWebDriver();
		m_webDriver.get("https://www.jackpotjoy.com");		
		
		try 
		{
			m_username = ConfigurationReader.getInstance().getString(QAExerciseConstans.USERNAME_KEY,"");
			m_password = ConfigurationReader.getInstance().getString(QAExerciseConstans.PASSWORD_KEY,"");
		} 
		catch (ConfigurationException e) 
		{
			throw e;			
		}
	}
	
	/***
	 * Testing the authentication feature steps. The validation is done by finding the homepage fields after the logout button was clicked.
	 */
	@Test
	public void authenticationFeature_01_test() 
	{			
		try
		{
			final AuthenticationPage authPage = PageFactory.initElements(m_webDriver, AuthenticationPage.class);
			authPage.getUserNameField().clear();
			authPage.getUserNameField().sendKeys(m_username);
			
			authPage.getPasswordField().clear();
			authPage.getPasswordField().sendKeys(m_password);
			
			authPage.getLoginButton().click();		
		}
		catch(org.openqa.selenium.NoSuchElementException e)
		{
			fail("Authentication elements were not found.");
			throw e;
		}
		
		try
		{
			final MainPage mainPage = PageFactory.initElements(m_webDriver, MainPage.class);
			mainPage.getLogOutLink().click();
		}
		catch(org.openqa.selenium.NoSuchElementException e)
		{
			fail("Main page elements were not found. User is not authenticated");
			throw e;
		}
		
		try
		{
			final AuthenticationPage authPageAfterLogOut = PageFactory.initElements(m_webDriver, AuthenticationPage.class);
			authPageAfterLogOut.getLoginButton().getTagName();
		}
		catch(org.openqa.selenium.NoSuchElementException e)
		{
			fail("Unexpected behavior in logout. Expected to be homepage.");
			throw e;
		}
		
		assertTrue("Unexpected behavior in Login logout operation.",true);
				
	}
	
	/***	 
	 * Testing the authentication feature steps. Username field is left empty. Expected that authentication to fail.
	 */
	@Test
	public void authenticationFeature_02_test()
	{		
		authenticateOperation("", m_password);
		
		try
		{
			final LoginPage loginPage = PageFactory.initElements(m_webDriver, LoginPage.class);	
			
			List<WebElement> messages = loginPage.getErrorDiv().findElements(By.cssSelector("ul > li"));
			if (messages.size() != 1)
			{
				fail("Unexpected erorr messaged count");
			}
			
			assertTrue("Unexpected behavior on failed login.",messages.get(0).getText().equals("Please enter your username or email address"));						
		}
		catch(org.openqa.selenium.NoSuchElementException e)	
		{
			fail("Fail, authentication error message not found.");
		}				
	}
	
	/***	 
	 * Testing the authentication feature steps. Password field is left empty. Expected that authentication to fail. 
	 */
	@Test
	public void authenticationFeature_03_test()
	{
		
		authenticateOperation(m_username, "");
		
		try
		{
			final LoginPage loginPage = PageFactory.initElements(m_webDriver, LoginPage.class);	
			
			List<WebElement> messages = loginPage.getErrorDiv().findElements(By.cssSelector("ul > li"));
			if (messages.size() != 1)
			{
				fail("Unexpected erorr messaged count");
			}
			
			assertTrue("Unexpected behavior on failed login.",messages.get(0).getText().equals("Please enter your password."));						
		}
		catch(org.openqa.selenium.NoSuchElementException e)	
		{
			fail("Fail, authentication error message not found.");
		}				
	}
	
	/***	 
	 * Testing the authentication feature steps. Password and username field are left empty. Expected that authentication to fail. 
	 */
	@Test
	public void authenticationFeature_04_test()
	{
		
		authenticateOperation("", "");
		
		try
		{
			final LoginPage loginPage = PageFactory.initElements(m_webDriver, LoginPage.class);	
			
			List<WebElement> messages = loginPage.getErrorDiv().findElements(By.cssSelector("ul > li"));
			if (messages.size() != 2)
			{
				fail("Unexpected erorr messaged count");
			}
			
			assertTrue("Unexpected behavior on failed login.", messages.get(0).getText().equals("Please enter your username or email address")
					 										&& messages.get(1).getText().equals("Please enter your password."));						
		}
		catch(org.openqa.selenium.NoSuchElementException e)	
		{
			fail("Fail, authentication error message not found.");
		}
	}
	
	/***	 
	 * Testing the authentication feature steps using bad user credentials. Expected that authentication to fail. 
	 */
	@Test
	public void authenticationFeature_05_test()
	{		
		authenticateOperation("testuser", "testpassword");
		
		try
		{
			final LoginPage loginPage = PageFactory.initElements(m_webDriver, LoginPage.class);	
			
			List<WebElement> messages = loginPage.getErrorDiv().findElements(By.cssSelector("ul > li"));
			if (messages.size() != 1)
			{
				fail("Unexpected erorr messaged count");
			}
			
			assertTrue("Unexpected behavior on failed login.",messages.get(0).getText().equals("Please try again as your username/email or password hasn't been recognised"));						
		}
		catch(org.openqa.selenium.NoSuchElementException e)	
		{
			fail("Fail, authentication error message not found.");
		} 
	}
	
	/***	 
	 * Testing the authentication feature steps using bad username with good password. Expected that authentication to fail. 
	 */
	@Test
	public void authenticationFeature_06_test()
	{
		authenticateOperation("testuser", m_password);

		try
		{
			final LoginPage loginPage = PageFactory.initElements(m_webDriver, LoginPage.class);	
			
			List<WebElement> messages = loginPage.getErrorDiv().findElements(By.cssSelector("ul > li"));
			if (messages.size() != 1)
			{
				fail("Unexpected erorr messaged count");
			}
				
			assertTrue("Unexpected behavior on failed login.",messages.get(0).getText().equals("Please try again as your username/email or password hasn't been recognised"));						
		}
		catch(org.openqa.selenium.NoSuchElementException e)	
		{
			fail("Fail, authentication error message not found.");
		}
	}
	
	/***	 
	 * Testing the authentication feature steps using good username with bad password. Expected that authentication to fail. 
	 */
	@Test
	public void authenticationFeature_07_test()
	{
		authenticateOperation(m_username, "testpassword" );

		try
		{
			final LoginPage loginPage = PageFactory.initElements(m_webDriver, LoginPage.class);	
		
			List<WebElement> messages = loginPage.getErrorDiv().findElements(By.cssSelector("ul > li"));
			if (messages.size() != 1)
			{
				fail("Unexpected erorr messaged count");
			}
				
			assertTrue("Unexpected behavior on failed login.",messages.get(0).getText().equals("Please try again as your username/email or password hasn't been recognised"));						
		}
		catch(org.openqa.selenium.NoSuchElementException e)	
		{
			fail("Fail, authentication error message not found.");
		}			
	}
	
	/***	 
	 * Testing the authentication feature steps. Username and password values are switched. Expected that authentication to fail. 
	 */
	@Test
	public void authenticationFeature_08_test()
	{
		authenticateOperation(m_password, m_username );
		
		try
		{
			final LoginPage loginPage = PageFactory.initElements(m_webDriver, LoginPage.class);	
		
			List<WebElement> messages = loginPage.getErrorDiv().findElements(By.cssSelector("ul > li"));
			if (messages.size() != 1)
			{
				fail("Unexpected erorr messaged count");
			}
				
			assertTrue("Unexpected behavior on failed login.",messages.get(0).getText().equals("Please try again as your username/email or password hasn't been recognised"));						
		}
		catch(org.openqa.selenium.NoSuchElementException e)	
		{
			fail("Fail, authentication error message not found.");
		}
	}
	
	/***
	 * Test utility method to reuse authentication steps
	 * @param username String
	 * @param password String
	 */
	private void authenticateOperation(final String username, final String password)
	{
		try
		{
			final AuthenticationPage authPage = PageFactory.initElements(m_webDriver, AuthenticationPage.class);
			authPage.getUserNameField().clear();
			authPage.getUserNameField().sendKeys(username);
			
			authPage.getPasswordField().clear();
			authPage.getPasswordField().sendKeys(password);
			
			authPage.getLoginButton().click();
		}
		catch(org.openqa.selenium.NoSuchElementException e)
		{
			fail("Authentication elements were not found.");
			throw e;
		}		
	}
	
	/***
	 * Cleaning up by shutting down the web driver
	 */
	@After
	public void tearDown()
	{
		m_webDriver.quit();
		m_username = "";
		m_password = "";
		
	}
	
	private WebDriver m_webDriver;
	private String m_username=null; 
	private String m_password=null;
}
