/**
 *  This package is containing the test cases for Authentication feature
 * @author Luca Dragos
 */
package com.gamesys.qaexercise.authentication.tests;