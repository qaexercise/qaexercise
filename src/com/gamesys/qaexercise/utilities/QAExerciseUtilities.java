package com.gamesys.qaexercise.utilities;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.commons.configuration.ConfigurationException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

/***
 * This class contains a set of utilities methods used in the QA exercise project
 * @author Luca Dragos
 */
public class QAExerciseUtilities 
{
	/***
	 * Returns a web driver as specified in file configuration
	 * @return WebDriver
	 * @throws ConfigurationException 
	 * @throws Exception
	 */
	public static WebDriver getDefaultWebDriver() throws ConfigurationException
	{
		final String stringWebDriver = ConfigurationReader.getInstance().getString(QAExerciseConstans.DEFAULT_BROWSER_KEY,QAExerciseConstans.DEFAULT_BROWSER);		
		if (SupportedBrowsersEnum.CHROME.getEnumValue().equals(stringWebDriver))
		{
			return getChromeDriver();
		}
			
		//Firefox browse is set as default browser 
		return getFirefoxDriver();
	}
	
	/**
	 * Default initialization of the Firefox web driver
	 * @return WebDriver
	 */
	private static WebDriver getFirefoxDriver()
	{
		WebDriver driver = null;
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(QAExerciseConstans.IMPLICIT_TIME_OUT_SECONDS, TimeUnit.SECONDS);
		return driver;
	}
	
	/**
	 * Default initialization of the Chrome web driver
	 * @return WebDriver
	 */
	private static WebDriver getChromeDriver()
	{				
		File file = new File(getChromeDriverPath());
		System.setProperty(QAExerciseConstans.CHROME_DRIVER_SYSTEM_PROP, file.getAbsolutePath());
		
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setJavascriptEnabled(true);
		
		WebDriver driver = new ChromeDriver(capabilities);
		driver.manage().window().maximize();
		
		return driver;
	}
	
	/***
	 * Creating the Chrome driver path based on OS type
	 * @return String
	 */
	private static String getChromeDriverPath()
	{
		String osName = System.getProperty(QAExerciseConstans.OS_NAME);
		String osArch = System.getProperty(QAExerciseConstans.OS_ARCH);		
		
		if(osName.toLowerCase().contains(OSEnum.WINDOWS.getEnumValue()))
		{
			return QAExerciseConstans.CHROME_DRIVER_PATH_WIN;
		}
		else 
		{
			if(osName.toLowerCase().contains(OSEnum.MAC.getEnumValue()))
			{
				return QAExerciseConstans.CHROME_DRIVER_PATH_MAC;
			}
			else
			{
				if(osName.toLowerCase().contains(OSEnum.LINUX.getEnumValue()))
				{
					if(osArch.contains(OSTypeEnum.Bit32.getEnumValue()))
					{
						return QAExerciseConstans.CHROME_DRIVER_PATH_LINUX32;
					}
					else 
						if(osArch.contains(OSTypeEnum.Bit64.getEnumValue()))
						{
							return QAExerciseConstans.CHROME_DRIVER_PATH_LINUX64;
						} 			
				}
			}
		}
		
		return null;
	}
}
