/**
 * This package is used to host the utilities classes of this project.
 * example: configuration readers, random guid generators, constants class,etc.
 * 
 * @author Luca Dragos
 */
package com.gamesys.qaexercise.utilities;