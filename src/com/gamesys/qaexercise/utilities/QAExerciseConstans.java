package com.gamesys.qaexercise.utilities;

/***
 * This class contains the project constants.
 * @author Luca Dragos
 */
public class QAExerciseConstans 
{
	public final static String CONFIGURATION_FILE="Configurations.config";
	
	//BROWSER MAPPING KEY
	public final static String DEFAULT_BROWSER_KEY="defaultBrowser";
	public final static String DEFAULT_BROWSER="Firefox";
	public final static int IMPLICIT_TIME_OUT_SECONDS=60;
		
	//CHROME DRIVER 
	public final static String CHROME_DRIVER_PATH_WIN = "./../GamesysQAExercise/chromeDriver/chromedriver_win_2.7.exe";
	public final static String CHROME_DRIVER_PATH_MAC = "./../GamesysQAExercise/chromeDriver/chromedriver_mac_2.7";
	public final static String CHROME_DRIVER_PATH_LINUX32 = "./../GamesysQAExercise/chromeDriver/chromedriver_linux_32_2.7";
	public final static String CHROME_DRIVER_PATH_LINUX64 = "./../GamesysQAExercise/chromeDriver/chromedriver_linux_64_2.7";
	public final static String CHROME_DRIVER_SYSTEM_PROP="webdriver.chrome.driver";
	
	//OS PROPERTIES
	public final static String OS_NAME="os.name";
	public final static String OS_ARCH="os.arch";	
	
	//REGISTER PAGE MAPPING KEYS
	public final static String REGISTER_TITLE_FIELD_KEY="register.title";
	
	//USER MAPPIGN KEYS
	public final static String USERNAME_KEY = "username";		
	public final static String PASSWORD_KEY = "password";
	
	//REGISTRATION USER MAPPING KEYS
	public final static String SEED_USERNAME_KEY="seedUsername";
	public final static String SEED_PASSWORD_KEY="seedPassword";
	public final static String SEED_MAIL_KEY="seedMail";
	public final static String LONDON_POSTAL_CODE_KEY="londonPostalCode";
	public final static String LONDON_PHONE_NUMBER_KEY="londonPhoneNumber";
	public final static String MAIDEN_NAME_KEY="maindenName";
	public final static String SEED_UPPER_LIMIT_KEY="seedUpperLimit";
	public final static String SEED_LOWER_LIMIT_KEY="seedLowerLimit";
	
	public final static int DEFAULT_UPPER_LIMIT=8000;
	public final static int DEFAULT_LOWER_LIMIT=7000;
}
