package com.gamesys.qaexercise.utilities;

/***
 * Supported browsers enum
 * @author Luca Dragos
 */
public enum SupportedBrowsersEnum 
{	
	FIREFOX("Firefox"),
	CHROME("Chrome");		
	
	SupportedBrowsersEnum(final String browser)
	{
		m_browser = browser;
	}
	
	public String getEnumValue()
	{
		return m_browser;
	}
	
	
	private String m_browser;
}
