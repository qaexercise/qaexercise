package com.gamesys.qaexercise.utilities;

/***
 * Enumeration of operation systems based on CPU architecture
 * @author Luca Dragos
 */
public enum OSTypeEnum 
{
	Bit64("64"),
	Bit32("32");
	
	OSTypeEnum(final String type)
	{
		m_type = type;
	}
	
	public String getEnumValue()
	{
		return m_type;
	}
	
	
	private String m_type;
}
