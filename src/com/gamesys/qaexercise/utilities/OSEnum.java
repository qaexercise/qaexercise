package com.gamesys.qaexercise.utilities;

/***
 * Enumeration of Operation Systems
 * @author Luca Dragos
 */
public enum OSEnum 
{
	WINDOWS("windows"),
	MAC("mac"),
	LINUX("linux");
	
	OSEnum(final String osName)
	{
		m_osName = osName;
	}
	
	public String getEnumValue()
	{
		return m_osName;
	}
	
	
	private String m_osName;
}
