package com.gamesys.qaexercise.utilities;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/***
 * Singleton configuration reader. It reads configuration properties based on key values
 * @author Luca Dragos
 */
public class ConfigurationReader extends PropertiesConfiguration 
{

	/***
	 * Private constructor
	 * @param configFile File Path
	 * @throws ConfigurationException
	 */
	private ConfigurationReader(final String configFile) throws ConfigurationException
	{
		super(configFile);
	}
	
	/***
	 * Retrieves the instance of ConfigurationReader
	 * @return DemoCasesConfig object
	 * @throws ConfigurationException
	 */
	public static synchronized ConfigurationReader getInstance() throws ConfigurationException 
	{
		if (s_config == null)
		{
			s_config = new ConfigurationReader(QAExerciseConstans.CONFIGURATION_FILE);
		}
		 
		return s_config;	
	}	
	
	//private members
	private static ConfigurationReader s_config;	
}
