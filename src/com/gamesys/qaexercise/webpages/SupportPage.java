package com.gamesys.qaexercise.webpages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/***
 * This class contains the Selenium wrappers for the web ui controls of the Support page
 * @author Luca Dragos
 */
public class SupportPage 
{		
	public WebElement getLiveChatButton() 
	{
		return m_liveChatButton;
	}
	
	@FindBy(id = "liveChat")
	private WebElement m_liveChatButton;
}
