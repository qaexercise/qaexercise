package com.gamesys.qaexercise.webpages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/***
 * This class contains the Selenium wrappers for the web ui controls for the Login page 
 * @author Luca Dragos
 */
public class LoginPage 
{
	public WebElement getErrorBanner()
	{
		return m_errorBanner;
	}
	
	public WebElement getErrorDiv()
	{
		return m_errorDiv;
	}
	
	@FindBy(css = "div.t-error")
	private WebElement m_errorDiv;
	
	@FindBy(css = "div.t-banner")
	private WebElement m_errorBanner;
}
