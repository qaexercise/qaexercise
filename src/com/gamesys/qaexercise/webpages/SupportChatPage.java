package com.gamesys.qaexercise.webpages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/***
 * This class contains the Selenium wrappers for the web ui controls of the Live chat page
 * @author Luca Dragos
 */
public class SupportChatPage 
{
	public WebElement getFirstNameField() 
	{
		return m_firstNameField;
	}

	public WebElement getLastNameField() 
	{
		return m_lastNameField;
	}

	public WebElement getEmailField() 
	{
		return m_emailField;
	}
	
	@FindBy(id = "rn_TextInput_10_first_name")
	private WebElement m_firstNameField;
	
	@FindBy(id = "rn_TextInput_12_last_name")
	private WebElement m_lastNameField;
	
	@FindBy(id = "rn_TextInput_14_email")
	private WebElement m_emailField;
}
