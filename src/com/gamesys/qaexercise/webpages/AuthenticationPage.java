package com.gamesys.qaexercise.webpages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/***
 * This class contains the Selenium wrappers for the web ui controls for the Authentication Page page
 * @author Luca Dragos
 */
public class AuthenticationPage 
{
	public WebElement getUserNameField()
	{
		return m_userNameField;
	}
	
	public WebElement getPasswordField()
	{
		return m_passwordField;
	}
	
	public WebElement getLoginButton()
	{
		return m_logInButton;
	}
	
	@FindBy(id = "usernameField")
	private WebElement m_userNameField;
	
	@FindBy(id = "passwordField")
	private WebElement m_passwordField;
	
	@FindBy(id = "loginSubmit")
	private WebElement m_logInButton;		
}
