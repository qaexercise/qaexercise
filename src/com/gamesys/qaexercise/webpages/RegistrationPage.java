package com.gamesys.qaexercise.webpages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/***
 * This class contains the Selenium wrappers for the web ui controls for the registration page
 * @author Luca Dragos
 */
public class RegistrationPage 
{
	public WebElement getTitle()
	{
		return m_title;
	}
	
	public WebElement getUserName() 
	{
		return m_userName;
	}

	public WebElement getPassword() 
	{
		return m_password;
	}

	public WebElement getPasswordConfirmation() 
	{
		return m_passwordConfirmation;
	}

	public WebElement getEmail() 
	{
		return m_email;
	}

	public WebElement getConfirmEmail() 
	{
		return m_confirmEmail;
	}

	public WebElement getFirstName() 
	{
		return m_firstName;
	}

	public WebElement getSurname() 
	{
		return m_surname;
	}

	public WebElement getDayOfBirth() 
	{
		return m_dayOfBirth;
	}

	public WebElement getMonthOfBirth() 
	{
		return m_monthOfBirth;
	}

	public WebElement getYearOfBirth() 
	{
		return m_yearOfBirth;
	}

	public WebElement getPostalCode() 
	{
		return m_postalCode;
	}

	public WebElement getFindAddressButton() 
	{
		return m_findAddressButton;
	}

	public WebElement getPhoneNumber() 
	{
		return m_phoneNumber;
	}

	public WebElement getSecurityAnswer() 
	{
		return m_securityAnswer;
	}

	public WebElement getTermsAndConditionsCheck() 
	{
		return m_termsAndConditionsCheck;
	}

	public WebElement getSubmitButton() 
	{
		return m_submitButton;
	}
			
	@FindBy(id = "userName")
	private WebElement m_userName;
	
	@FindBy(id = "plainText")
	private WebElement m_password;
	
	@FindBy(id = "confirm")
	private WebElement m_passwordConfirmation;
	
	@FindBy(id = "email")
	private WebElement m_email;
	
	@FindBy(id = "confirmEmail")
	private WebElement m_confirmEmail;
	
	@FindBy(id = "salutation")
	private WebElement m_title;
	
	@FindBy(id = "firstName")
	private WebElement m_firstName;
	
	@FindBy(id = "surname")
	private WebElement m_surname;
	
	@FindBy(id = "day")
	private WebElement m_dayOfBirth;
	
	@FindBy(id = "month")
	private WebElement m_monthOfBirth;
	
	@FindBy(id = "year")
	private WebElement m_yearOfBirth;
	
	@FindBy(id = "postcodelookup")
	private WebElement m_postalCode;	
	
	@FindBy(css = "#findAddressLink > span")
	private WebElement m_findAddressButton;
	
	@FindBy(id = "phoneNumber")
	private WebElement m_phoneNumber;
	
	@FindBy(id = "securityAnswer")
	private WebElement m_securityAnswer;
		
	@FindBy(id = "termsAndConditions")
	private WebElement m_termsAndConditionsCheck;
	
	@FindBy(id = "submit_1")
	private WebElement m_submitButton;
}
