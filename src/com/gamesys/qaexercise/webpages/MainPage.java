package com.gamesys.qaexercise.webpages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/***
 * This class contains the Selenium wrappers for the web ui controls for the Main page after authentication 
 * @author Luca Dragos
 */
public class MainPage 
{
	public WebElement getLogOutLink()
	{
		return m_logOutLink;
	}
	
	@FindBy(id = "pagelink")
	private WebElement m_logOutLink;	
}
