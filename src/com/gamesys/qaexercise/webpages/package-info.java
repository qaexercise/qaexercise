/**
 * This package contains the wrappers as Selenium objects for the web controls needed by the the unit tests  
 * @author Luca Dragos
 */
package com.gamesys.qaexercise.webpages;