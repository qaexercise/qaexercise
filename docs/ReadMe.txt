#-------------------------------------
# Running QA Exercise read me file
# AUTHOR: LUCA DRAGOS
#-------------------------------------

#--------------
# General information
#--------------
JDK used: 1.6
Builder: Ant

#--------------
# Framework used
#--------------
JUnit: JUnit v4.11
Selenium: Selenium v2.32
		  Chrome Driver v2.7

#-------------- 
# Configuration
#--------------
QA Exercise project uses a configuration file called Configurations.config which is in the main directory. 

The most important parameter from configuration file is the "defaultBrowser" which designates which driver will be used for tests. 
By default is set to "Firefox". To run tests under Chrome driver update the parameter with "Chrome" value and run the tests.   

Example of defaultBrowser parameter:

#Default values for parameter <defaultBrowser> : Firefox, Chrome
defaultBrowser=Firefox

Rest of the parameters in the configuration file are used for user specific data.

#--------------
# Running the tests
#--------------
To run the test we execute the below command in the project root directory ( next to build.xml file ). All path are relative to project root directory.  

	ant -buildfile build.xml

Each run will create a test directory with name ui_test_report_yyyy_MM_dd_HH_mm_ss where will be deposit the test results.



  
